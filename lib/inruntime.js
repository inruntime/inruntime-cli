#!/usr/bin/env node

var program = require('commander');
var sys = require('sys')
var exec = require('child_process').exec;
var fs = require('fs');
var colors = require('colors');
var Promise = require('bluebird');

program
  .version('0.4.2')
  .option('-m, --commit [changes]', 'commit local changes, pull team changes and push to repository')
  .option('-s, --stage', 'push to STAGING environment')
  .option('-d, --deploy', 'push to LIVE environment')
  .option('-S, --setup', 'force setup of git remotes')
  .option('-c, --clean', 'clean and reinstall local npm_modules')
  .option('-v, --verbose', 'verbose output')
  .option('-b, --build', 'build files with gulp')
  .option('-l, --log [appname]', 'tail heroku log messages')
  .option('-f, --flush', 'flush cloudflare cache')
  .option('-p, --pull', 'pull changes from cloud repository and update dependencies')
  .option('-r, --run', 'run the application locally')

  //Use this option to deploy to other apps
  .option('-a, --app [appname]', 'push to app')
  
  .parse(process.argv);

console.log(' ');
console.log('-= INRUNTIME CLI =-'.yellow.inverse);

var fns = [];
var command = [];
var config = {};
var app = 'default';

function execute() {
	if (program.clean) {
		console.log('WILL CLEAN AND REINSTALL LOCAL NPM DEPENDENCIES'.yellow.underline);
		command.push('npm cache clean');
		command.push('rm -rf node_modules');
		command.push('npm install');
	}
	if (program.build) {
		console.log('WILL BUILD'.yellow.underline);
		command.push('gulp'); 
	}
	if (program.commit) {
		if(program.commit == true) program.commit = 'various changes';
		if(!program.build && config.gulp) { 
			console.log('WILL BUILD'.yellow.underline);
			command.push('gulp'); 
		}
		console.log('WILL SEND TO REPOSITORY'.yellow.underline);
		command.push('git add -A');
		command.push('git commit -m "'+program.commit+'"');
		command.push('git pull origin master');
		command.push('git push origin master');
	}
	if (program.stage) {
		console.log('WILL DEPLOY TO '.yellow.underline+program.app.yellow.underline+'-STAGING'.yellow.underline);
		command.push('git push -f '+program.app+'-staging HEAD:master');
	}
	if (program.deploy) { 
		console.log('WILL DEPLOY TO'.yellow.underline+program.app.yellow.underline+'-LIVE'.yellow.underline);
		command.push('git push '+program.app+'-live master');
	}
	if (program.pull) {
		console.log('WILL PULL CHANGES FROM ORIGIN AND UPDATE DEPENDENCIES');
		command.push('git pull origin master');
		command.push('npm install -d');
	}
	if (program.run) {
		console.log('WILL BUILD AND RUN THE APP LOCALLY');
		command.push('gulp');
		command.push('NODE_ENV=development node app.js');
	}
	if (program.flush) {
		if(config.cloudflare) {
			console.log('WILL FLUSH CLOUDFLARE CACHE'.yellow.underline);
			console.log('cfg', config.cloudflare)
			command.push("curl https://www.cloudflare.com/api_json.html -d 'a=fpurge_ts' -d 'tkn="+config.cloudflare.token+"' -d 'email="+config.cloudflare.email+"' -d 'z="+config.cloudflare.domain+"' -d 'v=1'");
		} else {
			console.log(('WILL NOT FLUSH CACHE! Cloudflare key missing from the config file.').red.underline);
			console.log
		}
	}
	if (program.log) {
		console.log('WILL TAIL LOGS'.yellow.underline);
		if(program.log == true) program.log = config.logging || 'undefined';
		command.push('heroku logs --app '+program.log+' -t');
	}
	if (!command.length && !fns.length) { 
		program.help();
	} else {
		console.log('Running...'.yellow);
		Promise.all(fns).then(function(results){
		    if(program.verbose) {
				console.log(('Executing: '+ command.join('; ')).yellow);
			} else {
				console.log('Executing commands...'.yellow);
			}
			var ls = exec(command.join('; '), { encoding: 'utf8' });
			ls.stdout.on('data', function (data) {
			  process.stdout.write(data);
			});
			ls.stderr.on('data', function (data) {
			  process.stdout.write(data);
			});
			ls.on('exit', function (code) {
			  console.log(('\n'+'Bye bye! (code '+code+') ').yellow);
			});
		}).catch(function(e){
		    console.log(('FATAL ERROR: '+e).red.inverse);
			process.exit(1);
		});
	}
};

if (fs.existsSync('.inruntime')) {
	var tmp = fs.readFileSync('.inruntime','utf8');
	try {
		config = JSON.parse(tmp);
		var remote_result = [];
		var remote_check = exec('git remote', function (error, stdout, stderr) {
			if (error) return console.log('ERROR:', error);
			 	remote_result = stdout.split('\n');
			if (program.app && program.app !='' && program.app != true) {
				app = program.app;
			} else {
				program.app = app;
			}
			for (var remote in config.remote) {
				if(config.remote[remote] && config.remote[remote] != '' && config.remote[remote] != " " && remote.indexOf(app) != -1) {
					var envs = config.remote[remote]
					 for (var env in envs){
				 		if(remote_result.indexOf(remote+'-'+env) == -1) {
							console.log('Setting up remote: '+ remote+'-'+env +' '+ envs[env]);
							command.push('git remote add '+remote+'-'+env +' '+ envs[env]);
						} else {
							if(program.setup) {
								console.log('Setting up remote: '+remote+'-'+env +' '+ envs[env]);
								command.push('git remote remove '+remote+'-'+env);
								command.push('git remote add '+remote+'-'+env+' '+ envs[env]);
							}
						}
					}
				}
			}
			execute();
		});

	} catch(e) {
		console.log(('FATAL ERROR: '+e).red.inverse);
		process.exit(1);
	}
} else {
	console.log(('FATAL ERROR: .inruntime config file was not found').red.inverse);
	process.exit(1);
}